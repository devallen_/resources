# [ir0nstone Binary Exploitation (GREAT GitBook!!!)](https://ir0nstone.gitbook.io/notes)

# Category Specific Training Recommendations

## Reverse Engineering
- Common Tools
    - GDB (command line)
    - objdump (command line)
    - ghidra (gui / decompilier)
    - radare2 (command line)
    - [ROPgadget](https://github.com/JonathanSalwan/ROPgadget)

## Binary Exploitation
- I would advise starting with a barebones version of ubuntu and downloading tools as you need them, you do not need to use kali to be *l33t*.
- Typical defenses and work arounds
    - **NX** (Non-executable stack)
        - ret2win
        - ret2libc
        - ROP
    - **ASLR** (Address Space Layout Randomization)
        - ROP
        - format string vulns
    - **PIE** (Position Independent Executable)
        - *easiest:* leak via format strings vuln
        - Brute force (32-bit typically, 64-bit takes a while)
        - After you get a leak or brute-force it, ret2libc
    - **RELRO** (Relocation Read-Only)
        - This just means the external functions are populated in the GOT (global offset table) and that section of memory is marked as *read-only*.
        - look up the difference between *partial* and *full* RELRO
    - **Stack Canary**
        - format string vulns
        - Use the heap (typically a more advanced topic, but kudos if you know)

- Common Tools (that **ASSIST** with exploitation)
    - pwntools
        - *note: this is helpful to start with, but you should try to get comfortable with barebones python and its 'out-of-the-box' functionality*
    - ROPgadget
        - show all the available gadgets in a binary
        - *note: can also run on libc to get every gadget you could need*
- Challenges
    - If you don't know what a buffer overflow is
        - *disclaimer: during fall of my 2/C I knew nothing, if you are at this point (like I was), you can still get to where you need to be. You just need to want it bad enough.*
        - [CTFLearn](https://ctflearn.com/)
            - challenges > binary 
                - Simple bof
                - Favorite Color
                - RIP my bof
            - I liked these challenges, because the executable itself shows you the stack and what you're writing to each memory location after you send input.
    - Return Oriented Programming
        - [ROP Emporium](https://ropemporium.com/)
            - Do literally all of them
            - Architectures:
                - x86_64
                - x86
                - ARM
            - *note: calling conventions are important*
            - *note^2: do not use tools to craft your chains, learn to make them yourself*
    - Format String Bugs
        - [how2fsb](https://github.com/Caesurus/how2fsb)
            - Format strings are a little difficult to grasp, this will hopefully help.
    - Various 
        - [Protostar](https://exploit.education/protostar)
            - Stack: 0-7
            - Format: 0-4
        - [picoCTF 2018](https://tcode2k16.github.io/blog/posts/picoctf-2018-writeup/binary-exploitation/)
            - Although pico is no longer hosting *picoCTF 2018*, the challenges are still available at the link above
            - I believe these challenges helped drive **ALOT** of the concepts home for me 
        - [rpisec](http://security.cs.rpi.edu/courses/binexp-spring2015/)
            - Setting up the enviornment is a challenge in itself, if you can't figure it out on your own, reach out and let me know
            - These challenges are great for really testing your knowledge
        - [247ctf](https://247ctf.com/)
            - Some good `pwnable` challenges here
        - [nightmare](https://guyinatuxedo.github.io/00-intro/index.html)
            - [challenges](https://github.com/guyinatuxedo/nightmare) (clone the repo)
            - A great way to get scenario specific challenges
- Do not be afraid to go back repeat challenges, **ESPECIALLY** if you struggled on them and used the write-ups to help solve them. The best and only way to get better is to keep practicing. I often return to a lot of these challenges to ensure I can still do the low-level basic things. If you ever wanna talk about any of this, I am always available.
- I am nowhere an expert, however, I went from knowing literally nothing, to knowning *a little*.
- *note: this is a resource to **hopefully** prepare you better than I was.*
